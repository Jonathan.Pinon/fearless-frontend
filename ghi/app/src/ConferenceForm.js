import React, { useEffect, useState } from 'react';

function ConferenceForm() {
  const [locations, setLocations] = useState([])

  //Notice that we can condense all formData
  //into one state object
  const [formData, setFormData] = useState({
    name: '',
    starts: '',
    ends: '',
    description: '',
    max_presentations: '',
    max_attendees: '',
    location: '',
  })

  const fetchData = async () => {
    const url = 'http://localhost:8000/api/locations/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setLocations(data.locations);
    }
  }

  useEffect(() => {
    fetchData();
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();

    const url = 'http://localhost:8000/api/conferences/';

    const fetchConfig = {
      method: "post",
      //Because we are using one formData state object,
      //we can now pass it directly into our request!
      body: JSON.stringify(formData),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(url, fetchConfig);
    
    if (response.ok) {
      //The single formData object 
      //also allows for easier clearing of data
      setFormData({
        name: '',
        starts: '',
        ends: '',
        description: '',
        max_presentations: '',
        max_attendees: '',
        location: '',
      });
    }
  }

  //Notice that we can also replace multiple form change
  //eventlistener functions with one
  const handleFormChange = (e) => {
    const value = e.target.value;
    const inputName = e.target.name;

    //We can condense our form data event handling
    //into on function by using the input name to update it

    setFormData({
      //Previous form data is spread (i.e. copied) into our new state object
      ...formData,

      //On top of the that data, we add the currently engaged input key and value
      [inputName]: value 
    });
  }

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new conference</h1>
          <form onSubmit={handleSubmit} id="create-conference-form">

            <div className="form-floating mb-3">
              {/* <!-- Now, each field in our form references the same function --> */}
              <input onChange={handleFormChange} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
              <label htmlFor="name">Name</label>
            </div>

            <div className="form-floating mb-3">
              <input onChange={handleFormChange} placeholder="Starts" required type="date" name="starts" id="starts" className="form-control" />
              <label htmlFor="starts">Starts</label>
            </div>

            <div className="form-floating mb-3">
              <input onChange={handleFormChange} placeholder="Ends" required type="date" name="ends" id="ends" className="form-control" />
              <label htmlFor="ends">Ends</label>
            </div>

            <div className="mb-3">
              <label htmlFor="description">Description</label>
              <textarea onChange={handleFormChange} id="description" rows="3" name="description" className="form-control"></textarea>
            </div>

            <div className="form-floating mb-3">
              <input onChange={handleFormChange} placeholder="Maximum presentations" required type="number" name="max_presentations" id="max_presentations" className="form-control" />
              <label htmlFor="max_presentations">Maximum presentations</label>
            </div>

            <div className="form-floating mb-3">
              <input onChange={handleFormChange} placeholder="Maximum attendees" required type="number" name="max_attendees" id="max_attendees" className="form-control" />
              <label htmlFor="max_attendees">Maximum attendees</label>
            </div>

            <div className="mb-3">
              <select onChange={handleFormChange} required name="location" id="location" className="form-select">
                <option value="">Choose a location</option>
                {locations.map(location => {
                  return (
                    <option key={location.id} value={location.id}>{location.name}</option>
                  )
                })}
              </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  )
}

export default ConferenceForm;
// import React, { useEffect, useState } from 'react';

// function ConferenceForm(props){
//     const [locations, setLocations] = useState([]);
    
//     const [location, setLocation] = useState('');
//     const handleLocationChange = (event) => {
//         const value = event.target.value;
//         setLocation(value);
//     }

//     const [name, setName] = useState('');
//     const handleNameChange = (event) => {
//         const value = event.target.value;
//         setName(value);
//     }

//     const [start, setStart] = useState('');
//     const handleStartChange = (event) => {
//         const value = event.target.value;
//         setStart(value);
//     }

//     const [end, setEnd] = useState('');
//     const handleEndChange = (event) => {
//         const value = event.target.value;
//         setEnd(value);
//     }

//     const [description, setDescription] = useState('');
//     const handleDescriptionChange = (event) => {
//         const value = event.target.value;
//         setDescription(value);
//     }

//     const [maxPresentations, setMaxPresentations] = useState('');
//     const handlePresentationChange = (event) => {
//         const value = event.target.value;
//         setMaxPresentations(value);
//     }

//     const [maxAttendees, setMaxAttendees] = useState('');
//     const handleAttendeeChange = (event) => {
//         const value = event.target.value;
//         setMaxAttendees(value);
//     }

//     const handleSubmit = async (event) => {
//         event.preventDefault();
      
//         // create an empty JSON object
//         const data = {};
      
//         data.location = location;
//         data.name = name;
//         data.starts = start;
//         data.ends = end;
//         data.description = description;
//         data.max_presentations = maxPresentations;
//         data.max_attendees = maxAttendees;
      
//         console.log(data);

//         const locationUrl = 'http://localhost:8000/api/conferences/';
//         const fetchConfig = {
//             method: "post",
//             body: JSON.stringify(data),
//             headers: {
//             'Content-Type': 'application/json',
//             },
//         };

//         const response = await fetch(locationUrl, fetchConfig);
//         if (response.ok) {
//             const newLocation = await response.json();
//             console.log(newLocation);
//             setLocation('');
//             setName('');
//             setStart('');
//             setEnd('');
//             setDescription('');
//             setMaxPresentations('');
//             setMaxAttendees('');

//         }
//     }

//     const fetchData = async () => {
//     const url = 'http://localhost:8000/api/locations/';
//     const response = await fetch(url);

//     if (response.ok) {
//         const data = await response.json();
//         setLocations(data.locations)
//         }
//     }
//     useEffect(() => {
//         fetchData();
//     }, []);

//     return(
//         <div className="row">
//         <div className="offset-3 col-6">
//           <div className="shadow p-4 mt-4">
//             <h1>Create a new Conference</h1>
//             <form onSubmit={handleSubmit} id="create-conference-form">
//               <div className="form-floating mb-3">
//                 <input onChange={handleNameChange} value={name} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
//                 <label htmlFor="name">Name</label>
//               </div>
//               <div className="form-floating mb-3">
//                 <input onChange={handleStartChange} value={start} type="date" id="starts" name="starts" className="form-control" />
//                 <label htmlFor="starts">Starts</label>
//               </div>
//               <div className="form-floating mb-3">
//                 <input onChange={handleEndChange} value={end} placeholer="Ends" type="date" id="ends" name="ends" className="form-control" />
//                 <label htmlFor="ends">Ends</label>
//               </div>
//               <div className="mb-3">
//                 <label htmlFor="description">Description</label>
//                 <textarea onChange={handleDescriptionChange} value={description} placeholder="" required type="text" id="description" name="description" className="form-control"></textarea>
//               </div>
//               <div className="form-floating mb-3">
//                 <input onChange={handlePresentationChange} value={maxPresentations} placeholder="Max Presentations" required type="number" name="max_presentations" id="max_presentations" className="form-control" />
//                 <label htmlFor="max_presentations">Maximum presentations</label>
//               </div>
//               <div className="form-floating mb-3">
//                 <input onChange={handleAttendeeChange} value={maxAttendees} placeholder="Max Attendees" required type="number" name="max_attendees" id="max_attendees" className="form-control" />
//                 <label htmlFor="max_attendees">Maximum attendees</label>
//               </div>
//               <div className="mb-3">
//                 <select onChange={handleLocationChange} value={location} required id="location" name="location" className="form-select">
//                   <option value="">Choose a Location</option>
//                         {locations.map(location => {
//                             return (
//                             <option key={location.id} value={location.id}>
//                                 {location.name}
//                             </option>
//                             );
//                         })}
//                 </select>
//               </div>
//               <button className="btn btn-primary">Create</button>
//             </form>
//           </div>
//         </div>
//       </div>
//     );
// }

// export default ConferenceForm;